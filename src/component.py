'''
Template Component main class.

'''

import logging
import os
import sys
import csv
import json
from pprint import pprint
from pathlib import Path
from datetime import datetime

from kbc.env_handler import KBCEnvHandler

# #### Keep for debug
KEY_DEBUG = 'debug'

MANDATORY_PARS = ['print_rows']
MANDATORY_IMAGE_PARS = []

APP_VERSION = '1.0.0'

class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        # for easier local project setup
        default_data_dir = Path(__file__).resolve().parent.parent.joinpath('data').as_posix() \
            if not os.environ.get('KBC_DATADIR') else None

        KBCEnvHandler.__init__(self, MANDATORY_PARS, log_level=logging.DEBUG if debug else logging.INFO, data_path=default_data_dir)
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True
        if debug:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config(MANDATORY_PARS)
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)
        # ####### EXAMPLE TO REMOVE
        # intialize instance parameteres

        # ####### EXAMPLE TO REMOVE END

    def run(self):
        '''
        Main execution code
        '''

        config = self.cfg_params

        SOURCE_FILE_PATH = Path(self.get_input_tables_definitions()[0].full_path) # first table
        RESULT_FILE_PATH = Path(self.tables_out_path).joinpath('output.csv')

        PARAM_PRINT_LINES = config['print_rows']

        print('Running...')
        with open(SOURCE_FILE_PATH, 'r') as input, open(RESULT_FILE_PATH, 'w+', newline='') as out:
            reader = csv.DictReader(input)
            new_columns = reader.fieldnames
            new_columns.append('row_number')  # append row number col

            writer = csv.DictWriter(out, fieldnames=new_columns, lineterminator='\n', delimiter=',')
            writer.writeheader()
            for index, l in enumerate(reader):
                # add row number
                l['row_number'] = index

                # print line
                if PARAM_PRINT_LINES:
                    print(f'Printing line {index}: {l}')
                    
                writer.writerow(l)

        # writing manifest
        self.configuration.write_table_manifest(file_name=str(RESULT_FILE_PATH), destination='in.c-10-tasks.output2', primary_key=['row_number'], incremental=True)

        # state
        last_updated = self.get_state_file().get('last_updated')
        logging.info(f'Last updated: {last_updated}')
        self.write_state_file({'last_updated': str(datetime.now())})




"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
    except Exception as exc:
        logging.exception(exc)
        exit(1)
